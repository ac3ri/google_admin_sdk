from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import json

# Google Admin SDK - Explorer API
# https://developers.google.com/admin-sdk/reports/v1/reference/activities/list

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/admin.reports.audit.readonly']

FAILRED = '\033[91m'
ENDC = '\033[0m'

creds = None
# The file token.pickle stores the user's access and refresh tokens, and is
# created automatically when the authorization flow completes for the first
# time.
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server()
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('admin', 'reports_v1', credentials=creds)

number_of_results = 20


def api_login():
    print("The G Suite Login application's activity report:")
    results = service.activities().list(userKey='all', applicationName='login',
                                        # startTime='2018-01-25T09:40:34.533Z',
                                        maxResults=number_of_results).execute()
    activities_login = results.get('items', [])

    # if not activities_login:
    #     print('No logins found.')
    # else:
    #     # example output:
    #     # 2019-01-15T12:28:28.068Z: amontemayor@gocardless.com (login_success), IP: 217.138.59.34
    #     for activity in activities_login:
    #         # print(activity)
    #         if activity['events'][0]['name'] == 'login_failure':
    #             print((FAILRED + u'{0}: {1} ({2}), IP: {3}' + ENDC).format(activity['id']['time'],
    #                                                                        activity['actor']['email'],
    #                                                                        activity['events'][0]['name'],
    #                                                                        activity['ipAddress']))
    #         else:
    #             print(u'{0}: {1} ({2}), IP: {3}'.format(activity['id']['time'],
    #                                                     activity['actor']['email'],
    #                                                     activity['events'][0]['name'], activity['ipAddress']))

    data = json.dumps(activities_login, indent=4, sort_keys=True)
    print(data)
    # with open('google_api_login.json', 'w') as file:
    #     file.write(data)

    return print('--Completed "login" query.')


def api_admin():
    print("The Admin console application's activity reports:")
    results = service.activities().list(userKey='all', applicationName='admin',
                                        maxResults=number_of_results).execute()
    activities_admin = results.get('items', [])
    # for activity in activities_admin:
    #     try:
    #         print(u'{0}: {1} ({2}), IP: {3}'.format(activity['id']['time'],
    #                                                        activity['actor']['email'],
    #                                                        activity['events'], activity['ipAddress']))
    #     except Exception as e:
    #         print('Error: {}'.format(e))
    #         print(activity)

    data = json.dumps(activities_admin, indent=4, sort_keys=True)
    print(data)
    # with open('google_api_admin.json', 'w') as file:
    #     file.write(data)

    return print('--Completed "admin" query.')


def api_token():
    print("The G Suite Token application's activity reports:")
    results = service.activities().list(userKey='all', applicationName='token',
                                        maxResults=number_of_results).execute()
    activities_token = results.get('items', [])
    # for activity in activities_token:
    #     # print(activity)
    #     try:
    #         print(u'{0}: {1} ({2}), IP: {3}'.format(activity['id']['time'],
    #                                                        activity['actor'],
    #                                                        activity['events'], activity['ipAddress']))
    #     except Exception as e:
    #         print('Error: {}'.format(e))
    #         print(activity)

    data = json.dumps(activities_token, indent=4, sort_keys=True)
    print(data)
    # with open('google_api_token.json', 'w') as file:
    #     file.write(data)

    return print('--Completed "token" query.')


def api_user_accounts():
    print("The G Suite User Accounts application's activity reports:")
    results = service.activities().list(userKey='all', applicationName='user_accounts',
                                        maxResults=number_of_results).execute()

    activities_user_accounts = results.get('items', [])
    # for activity in activities_user_accounts:
    #     # print(activity)
    #     try:
    #         print(u'{0}: {1} ({2}), IP: {3}'.format(activity['id']['time'],
    #                                                        activity['actor'],
    #                                                        activity['events'], activity['ipAddress']))
    #     except Exception as e:
    #         print('Error: {}'.format(e))
    #         print(activity)
    #
    data = json.dumps(activities_user_accounts, indent=4, sort_keys=True)
    print(data)
    # with open('google_api_user_accounts.json', 'w') as file:
    #     file.write(data)

    return print('--Completed "user_accounts" query.')


def api_drive():
    print("The Google Drive application's activity:")
    results = service.activities().list(userKey='jindrich@gocardless.com', applicationName='drive',
                                        maxResults=number_of_results).execute()

    activities_drive = results.get('items', [])
    # for activity in activities_drive:
    #     # print(activity)
    #     try:
    #         print(u'{0}: {1} ({2}), IP: {3}'.format(activity['id']['time'],
    #                                                        activity['actor'],
    #                                                        activity['events'], activity['ipAddress']))
    #     except Exception as e:
    #         print('Error: {}'.format(e))
    #         print(activity)

    data = json.dumps(activities_drive, indent=4, sort_keys=True)
    print(data)
    # with open('google_api_drive.json', 'w') as file:
    #     file.write(data)

    return print('--Completed "drive" query.')


def main():
    """API calls of service.activities().list(params) using the Admin SDK Reports API.
    """

    # Call the Admin SDK Reports API - login
    api_login()

    # Call the Admin SDK Reports API - admin
    api_admin()

    # Call the Admin SDK Reports API - token
    api_token()

    # Call the Admin SDK Reports API - user_accounts
    api_user_accounts()

    # Call the Admin SDK Reports API - user_accounts
    api_drive()


if __name__ == '__main__':
    main()
